<?php

 namespace Framework\Reporter;

 class SysReporter
 {

     public function memoryUsage()
     {
	 return round(memory_get_peak_usage(false) / 1024 / 1024, 2);
     }

     public function cpuUsage()
     {
	 return (function_exists('sys_getloadavg')) ? sys_getloadavg()[0] : 0;
     }

 }
 