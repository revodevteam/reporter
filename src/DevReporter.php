<?php

namespace Framework\Reporter;

class DevReporter extends SysReporter {

    protected $memoryReport;
    protected $scriptReport;
    protected $sqlReport;

    public function __construct($startTime, $startMemory)
    {
        if (isDebug() === false) {
            return false;
        }

        $this->memoryReport = new \stdClass();
        $this->scriptReport = new \stdClass();
        $this->sqlReport = new \stdClass();
        $this->sqlReport->queries = [];

        $this->scriptReport->startTime = $startTime;
        $this->pushToMemoryStack('init', $startMemory);

        $this->pushToMemoryStack('config');
    }

    public function pushToMemoryStack(string $key, $value = false)
    {
        if ($value === false) {
            $value = $this->memoryUsage();
        }

        $this->memoryReport->usage[] = array('class' => $key, 'memory' => $value);
    }

    public function sqlDetails($details)
    {
        $this->sqlReport->queries[] = $details;
    }

    public function setCurrentController(string $controller)
    {
        $this->scriptReport->currentController = $controller;
    }

    public function setCurrentMethod(string $model)
    {
        $this->scriptReport->currentMethod = $model;
    }

    public function setCurrentView(string $view)
    {
        $this->scriptReport->currentView = $view;
    }

    public function getCurrentController()
    {
        if (isset($this->scriptReport->currentController)) {
            return $this->scriptReport->currentController;
        }
        return false;
    }

    public function getCurrentMethod()
    {
        if (isset($this->scriptReport->currentMethod)) {
            return $this->scriptReport->currentMethod;
        }
        return false;
    }

    public function getCurrentView()
    {
        if (isset($this->scriptReport->currentView)) {
            return $this->scriptReport->currentView;
        }
        return false;
    }

    public function isReportable()
    {
        return (strtoupper($_SERVER['REQUEST_METHOD'] ?? '') === 'GET' && (strpos(strtolower($_SERVER['HTTP_ACCEPT'] ?? ''), 'text/html') !== false) && strpos(php_sapi_name(), 'cli') === false);
    }

    public function __destruct()
    {

        if (isDebug() === false || $this->isReportable() === false) {
            return false;
        }

        $this->scriptReport->endTime = microtime(true);
        $memoryUsage = $this->memoryUsage();
        $processorUsage = $this->cpuUsage();
        $executionTime = ($this->scriptReport->endTime - $this->scriptReport->startTime);

        $currentController = $this->getCurrentController();
        $currentMethod = $this->getCurrentMethod();
        $currentViewFile = $this->getCurrentView();
        $totalSQlex = (!empty($this->sqlReport->queries)) ? count($this->sqlReport->queries) : 0;

        if ($processorUsage <= 2 && $memoryUsage <= 1 && $executionTime < 1) {
            $class = 'success';
        } else if ($processorUsage > 2 && $memoryUsage <= 2 && $executionTime <= 2) {
            $class = 'warning';
        } else {
            $class = 'danger';
        }
        ?>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-<?php echo $class; ?>">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <strong><u>Resource Usage Statistics Summary</u></strong>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <?php
                        //echo '<pre>';
                        echo 'Execution Time: ' . $executionTime . ' Seconds<br />';
                        echo 'Loaded Controller: ' . $currentController . '<br>';
                        echo 'Loaded Method: ' . $currentMethod . '<br>';
                        echo 'View File : ' . $currentViewFile . '<br>';
                        echo 'CPU Usage: ' . $processorUsage . ' %<br>';
                        echo 'Memory Usage: ' . $memoryUsage . ' MB<br>';
                        echo 'Total Query Executed: ' . $totalSQlex . ' SQL<br>';
                        //echo '</pre>';
                        ?>
                    </div>
                </div>
            </div>
            <?php
            $totalAvailableMemory = $memoryUsage;
            $currentMemoryUsage = 0;
            if ($memoryUsage <= 1) {
                $class = 'success';
            } else if ($memoryUsage < 2) {
                $class = 'warning';
            } else {
                $class = 'danger';
            }
            ?>
            <div class="panel panel-<?php echo $class; ?>">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <strong><u>Memory Usage Details</u></strong> (Total Usage: <?php echo $totalAvailableMemory; ?> MB)
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <?php
                        if (!empty($this->memoryReport->usage)) {
                            foreach ($this->memoryReport->usage as $usage) {
                                $module = $usage['class'];
                                $MemoryUsage = $usage['memory'];

                                $currentMemoryUsage = round($MemoryUsage - $currentMemoryUsage, 2);
                                $currentMemoryUsagePercentage = round(($currentMemoryUsage / $totalAvailableMemory) * 100, 2);
                                if ($currentMemoryUsagePercentage < 15) {
                                    $class = 'success';
                                } else if ($currentMemoryUsagePercentage < 30) {
                                    $class = 'info';
                                } else if ($currentMemoryUsagePercentage < 50) {
                                    $class = 'warning';
                                } else {
                                    $class = 'danger';
                                }
                                echo '
                                    <div class="progress" style="margin-bottom: 3px; display:flex;">
                                      <div class="progress-bar progress-bar-' . $class . '" style="width: ' . $currentMemoryUsagePercentage . '%; overflow: visible;white-space: nowrap; color:#333; text-align:left;">
                                      <span>' . $module . ' - ' . $currentMemoryUsage . 'MB (' . $currentMemoryUsagePercentage . '%)</span>
                                      </div>
                                    </div>
                                  ';
                                $currentMemoryUsage = $MemoryUsage;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
            if ($totalSQlex < 3) {
                $class = 'success';
            } else if ($totalSQlex <= 5) {
                $class = 'warning';
            } else {
                $class = 'danger';
            }
            ?>
            <div class="panel panel-<?php echo $class; ?>">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <strong><u>DB Query Statistics</u></strong>(Total <?php echo $totalSQlex; ?> Queries)
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <?php
                        echo '<pre>';
                        print_r($this->sqlReport);
                        echo '</pre>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}
